//更新首页logo
layui.use('upload', function() {

    var upload = layui.upload;
    //执行实例
    var uploadInst = upload.render({
        elem : '#pic1', //绑定元素
        url : '/common/sysFile/upload', //上传接口
        size : 3000,
        data: {method:'bgpic',originalPathName:'/files/baf3b1aa-1b47-4780-b599-123456.jpg'},
        accept : 'file',
        done : function(r) {

            layer.msg(r.msg);
            window.location.reload()

        },
        error : function(r) {
            layer.msg(r.msg);
        }
    });
});


//更新首页上背景
layui.use('upload', function() {

    var upload = layui.upload;
    //执行实例
    var uploadInst = upload.render({
        elem : '#pic2', //绑定元素
        url : '/common/sysFile/upload', //上传接口
        size : 3000,
        data: {method:'bgpic',originalPathName:'/files/fe833ece-077e-4af5-9fb7-c290afec1a90.jpg'},
        accept : 'file',
        done : function(r) {

            layer.msg(r.msg);
            window.location.reload()

        },
        error : function(r) {
            layer.msg(r.msg);
        }
    });
});

//更新首页下背景
layui.use('upload', function() {

    var upload = layui.upload;
    //执行实例
    var uploadInst = upload.render({
        elem : '#pic3', //绑定元素
        url : '/common/sysFile/upload', //上传接口
        size : 3000,
        data: {method:'bgpic',originalPathName:'/files/fe833ece-077e-4af5-9fb7-33456.jpg'},
        accept : 'file',
        done : function(r) {

            layer.msg(r.msg);
            window.location.reload()

        },
        error : function(r) {
            layer.msg(r.msg);
        }
    });
});

//更新详情页背景
layui.use('upload', function() {

    var upload = layui.upload;
    //执行实例
    var uploadInst = upload.render({
        elem : '#pic4', //绑定元素
        url : '/common/sysFile/upload', //上传接口
        size : 3000,
        data: {method:'bgpic',originalPathName:'/files/fe833ece-077e-4af5-9fb7-43456.jpg'},
        accept : 'file',
        done : function(r) {

            layer.msg(r.msg);
            window.location.reload()

        },
        error : function(r) {
            layer.msg(r.msg);
        }
    });
});

//更新新闻动态页背景
layui.use('upload', function() {

    var upload = layui.upload;
    //执行实例
    var uploadInst = upload.render({
        elem : '#pic5', //绑定元素
        url : '/common/sysFile/upload', //上传接口
        size : 3000,
        data: {method:'bgpic',originalPathName:'/files/fe833ece-077e-4af5-9fb7-53456.jpg'},
        accept : 'file',
        done : function(r) {

            layer.msg(r.msg);
            window.location.reload()

        },
        error : function(r) {
            layer.msg(r.msg);
        }
    });
});