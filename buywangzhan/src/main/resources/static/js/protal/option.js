
function edit(formId,fileId) {

   var queryData =  query(fileId);

    layer.open({
        title: '编辑',
        type:1,
        area: ['650px','550px'],
        content: $('#'+formId),
        maxmin:true
    });
    $('#'+formId).show();
    clearForm(formId);
    $('#'+formId).formEdit(queryData);
    $('#'+formId).formEdit({method:2});

}

function remove(fileId,coordinate,coordinate2) {

    layer.msg('您确认删除吗？', {
        time: 0 //不自动关闭
        ,btn: ['确认', '取消']
        ,yes: function(index){
            reallyRemove(fileId,coordinate,coordinate2);
        }
    });
}

function  reallyRemove(fileId,coordinate,coordinate2) {
    $.ajax({
        url : "/portal/remove",
        type : "post",
        async: false,
        data :{id:fileId,coordinate:coordinate,coordinate2:coordinate2},
        success : function(r) {
            if (r.code==0) {
                layer.msg(r.msg);
                window.location.reload();
            } else {
                layer.msg(r.msg, {
                    time: 0 //不自动关闭
                    ,btn: [ '知道了']
                });

            }
        }
    });


}

function reload(msg,formId) {
    // clearForm(formId);
    layer.closeAll('page');
    layer.msg('您好，'+msg+',您需要重新加载页面吗？', {
        time: 0 //不自动关闭
        ,btn: ['需要', '暂不需要']
        ,yes: function(){
            window.location.reload();
        }
    });

}

function add(formId,coordinate,coordinate2) {

    $('#'+formId).formEdit({coordinate:coordinate,coordinate2:coordinate2,method:1});
    layer.open({
        title: '新增',
        type:1,
        area: ['650px','550px']
        ,content: $('#'+formId),
        maxmin:true
    });
    $('#'+formId).show();
    clearForm(formId);
}

function query(fileId) {
    var reValue;
    $.ajax({
        url : "/portal/query",
        type : "post",
        async: false,
        data :{id:fileId},
        success : function(file) {
            if (file) {
                reValue =file;
            } else {
                layer.msg("可能是网络故障，请稍后重试或者联系buywangzhan管理员");
            }
        }
    });
    return reValue;
}

function reallyAdd(formId) {
    $.ajax({
        url : "/portal/save",
        type : "post",
        data :$('#'+formId).serialize(),
        success : function(r) {
            if (r.code == 0) {
                reload(r.msg,formId);
            } else {
                layer.msg(r.msg);
            }
        }
    });
}

function reallyEdit(formId) {
    $.ajax({
        url : "/portal/update",
        type : "post",
        data :$('#'+formId).serialize(),
        success : function(r) {
            if (r.code == 0) {

                reload(r.msg,formId);
            } else {
                layer.msg(r.msg);
            }
        }
    });
}

function ajaxSubmit(formId,method) {
    var me = $('#'+method).val();
    if(me==1){
        reallyAdd(formId);
    }else if(me==2){
        reallyEdit(formId);
    }


}

$.fn.formEdit = function(data){
    return this.each(function(){
        var input, name,fileDetail,fileNames;
        if(data == null){this.reset(); return; }
        fileDetail =  data.fileDetailDO;
        for(var i = 0; i < this.length; i++){
            input = this.elements[i];
            //checkbox的name可能是name[]数组形式
            name = (input.type == "checkbox")? input.name.replace(/(.+)\[\]$/, "$1") : input.name;
            if(data[name] == undefined){
                if(fileDetail){
                    if(name){
                     fileNames =     name.split('.');
                        if(fileNames.length>1){
                            input.value = fileDetail[fileNames[1]];
                        }
                    }

                }
                    continue;
            }

            switch(input.type){
                case "checkbox":
                    if(data[name] == ""){
                        input.checked = false;
                    }else{
                        //数组查找元素
                        if(data[name].indexOf(input.value) > -1){
                            input.checked = true;
                        }else{
                            input.checked = false;
                        }
                    }
                    break;
                case "radio":
                    if(data[name] == ""){
                        input.checked = false;
                    }else if(input.value == data[name]){
                        input.checked = true;
                    }
                    break;
                case "button": break;
                default: input.value = data[name];
            }
        }
    });
};

function clearForm(formId) {
    $(':input','#'+formId)
        .not(':button, :submit, :reset, :hidden')
        .val('')
        .removeAttr('checked')
        .removeAttr('selected');

}

