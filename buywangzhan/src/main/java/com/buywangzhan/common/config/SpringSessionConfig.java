package com.buywangzhan.common.config;

import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;

@ConditionalOnProperty(prefix = "buywangzhan", name = "spring-session-open", havingValue = "true")
public class SpringSessionConfig {

}
