package com.buywangzhan.common.dao;

import com.buywangzhan.common.domain.FileDetailDO;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

/**
 * 文件上传
 * @author chglee
 * @email 1992lcg@163.com
 * @date 2017-10-03 15:45:42
 */
@Mapper
public interface FileDetailDao {

	FileDetailDO get(Long id);

	FileDetailDO getBySysFileId(Long sysFileId);

	List<FileDetailDO> list(Map<String, Object> map);
	
	int count(Map<String, Object> map);
	
	int save(FileDetailDO fileDetail);
	
	int update(FileDetailDO fileDetail);
	
	int remove(Long id);
	
	int batchRemove(Long[] ids);
}
