package com.buywangzhan.common.service.impl;

import com.buywangzhan.common.dao.FileDao;
import com.buywangzhan.common.dao.FileDetailDao;
import com.buywangzhan.common.domain.FileDO;
import com.buywangzhan.common.domain.FileDetailDO;
import com.buywangzhan.common.service.FileService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.Map;


@Service
public class FileServiceImpl implements FileService {
    @Autowired
    private FileDao sysFileMapper;
    @Autowired
    private FileDetailDao sysFileDetailMapper;

    @Override
    public FileDO get(Long id) {
        return sysFileMapper.get(id);
    }

    @Override
    public FileDetailDO getBySysFileId(Long sysFileId) {
        return sysFileDetailMapper.getBySysFileId(sysFileId);
    }

    @Override
    public List<FileDO> list(Map<String, Object> map) {
        return sysFileMapper.list(map);
    }

    @Override
    public int count(Map<String, Object> map) {
        return sysFileMapper.count(map);
    }

    @Override
    public int save(FileDO sysFile) {
        int count = sysFileMapper.save(sysFile);
        FileDetailDO fileDetailDO = sysFile.getFileDetailDO();
        if (fileDetailDO != null && (StringUtils.isNoneBlank(fileDetailDO.getTitle()) || StringUtils.isNoneBlank(fileDetailDO.getUrl()) || StringUtils.isNoneBlank(fileDetailDO.getContent()))) {
            fileDetailDO.setSysFileId(sysFile.getId());
            count = sysFileDetailMapper.save(fileDetailDO);
        }

        return count;
    }

    @Override
    public int update(FileDO sysFile) {
        FileDetailDO fileDetailDO = sysFile.getFileDetailDO();
        if (fileDetailDO != null && (StringUtils.isNoneBlank(fileDetailDO.getTitle()) || StringUtils.isNoneBlank(fileDetailDO.getUrl()) || StringUtils.isNoneBlank(fileDetailDO.getContent()))) {
            fileDetailDO.setSysFileId(sysFile.getId());
            FileDetailDO fileDetail =   sysFileDetailMapper.getBySysFileId(sysFile.getId());
            if(fileDetail==null){

                    if(org.apache.commons.lang.StringUtils.isBlank(sysFile.getHref())){
                        if(org.apache.commons.lang.StringUtils.isNotBlank(fileDetailDO.getUrl())&& org.apache.commons.lang.StringUtils.isNotBlank(fileDetailDO.getUrl2())&& org.apache.commons.lang.StringUtils.isNotBlank(fileDetailDO.getUrl3())&& org.apache.commons.lang.StringUtils.isNotBlank(fileDetailDO.getUrl4())&& org.apache.commons.lang.StringUtils.isNotBlank(fileDetailDO.getUrl5())){
                            sysFile.setHref("/portal/detail/detail5/"+sysFile.getId());
                        }else if(org.apache.commons.lang.StringUtils.isNotBlank(fileDetailDO.getUrl())&& org.apache.commons.lang.StringUtils.isNotBlank(fileDetailDO.getUrl2())&& org.apache.commons.lang.StringUtils.isNotBlank(fileDetailDO.getUrl3())){
                            sysFile.setHref("/portal/detail/detail3/"+sysFile.getId());
                        }
                    }
                    fileDetailDO.setCreateDate(new Date());
                sysFileDetailMapper.save(fileDetailDO);
            }else{
                if(org.apache.commons.lang.StringUtils.isBlank(sysFile.getHref())){
                    if(org.apache.commons.lang.StringUtils.isNotBlank(fileDetailDO.getUrl())&& org.apache.commons.lang.StringUtils.isNotBlank(fileDetailDO.getUrl2())&& org.apache.commons.lang.StringUtils.isNotBlank(fileDetailDO.getUrl3())&& org.apache.commons.lang.StringUtils.isNotBlank(fileDetailDO.getUrl4())&& org.apache.commons.lang.StringUtils.isNotBlank(fileDetailDO.getUrl5())){
                        sysFile.setHref("/portal/detail/detail5/"+sysFile.getId());
                    }else if(org.apache.commons.lang.StringUtils.isNotBlank(fileDetailDO.getUrl())&& org.apache.commons.lang.StringUtils.isNotBlank(fileDetailDO.getUrl2())&& org.apache.commons.lang.StringUtils.isNotBlank(fileDetailDO.getUrl3())){
                        sysFile.setHref("/portal/detail/detail3/"+sysFile.getId());
                    }
                }
                sysFileDetailMapper.update(fileDetailDO);
            }

        }
        return sysFileMapper.update(sysFile);
    }

    @Override
    public int remove(Long id) {
        return sysFileMapper.remove(id);
    }

    @Override
    public int batchRemove(Long[] ids) {
        return sysFileMapper.batchRemove(ids);
    }

}
