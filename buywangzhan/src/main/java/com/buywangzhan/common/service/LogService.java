package com.buywangzhan.common.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.buywangzhan.common.domain.LogDO;
import com.buywangzhan.common.domain.PageDO;
import com.buywangzhan.common.utils.Query;
@Service
public interface LogService {
	PageDO<LogDO> queryList(Query query);
	int remove(Long id);
	int batchRemove(Long[] ids);
}
