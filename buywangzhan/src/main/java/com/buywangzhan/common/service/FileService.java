package com.buywangzhan.common.service;

import com.buywangzhan.common.domain.FileDO;
import com.buywangzhan.common.domain.FileDetailDO;

import java.util.List;
import java.util.Map;

/**
 * 文件上传
 * 
 * @author chglee
 * @email 1992lcg@163.com
 * @date 2017-09-19 16:02:20
 */
public interface FileService {
	
	FileDO get(Long id);

	FileDetailDO getBySysFileId(Long sysFileId);
	
	List<FileDO> list(Map<String, Object> map);
	
	int count(Map<String, Object> map);
	
	int save(FileDO sysFile);
	
	int update(FileDO sysFile);
	
	int remove(Long id);
	
	int batchRemove(Long[] ids);
}
