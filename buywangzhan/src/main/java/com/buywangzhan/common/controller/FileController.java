package com.buywangzhan.common.controller;

import com.buywangzhan.common.config.BuywangzhanConfig;
import com.buywangzhan.common.domain.FileDO;
import com.buywangzhan.common.service.FileService;
import com.buywangzhan.common.utils.*;
import com.buywangzhan.system.enums.FileTypeEnum;
import org.apache.catalina.servlet4preview.http.HttpServletRequest;
import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 文件上传&t图片上传
 *
 * @author chglee
 * @email 1992lcg@163.com
 * @date 2017-09-19 16:02:20
 */
@Controller
@RequestMapping("/common/sysFile")
public class FileController extends BaseController {

    @Autowired
    private FileService sysFileService;

    @Autowired
    private BuywangzhanConfig buywangzhanConfig;

    @GetMapping()
    @RequiresPermissions("common:sysFile:sysFile")
    String SysFile(Model model) {
        Map<String, Object> params = new HashMap<>();
        return "common/file/file";
    }

    @ResponseBody
    @GetMapping("/list")
    @RequiresPermissions("common:sysFile:sysFile")
    public PageUtils list(@RequestParam Map<String, Object> params) {
        // 查询列表数据
        Query query = new Query(params);
        String str_type = (String) params.get("type");
        int type = new Integer(StringUtils.isBlank(str_type) ? "-1" : (String) params.get("type"));
        if (type == FileTypeEnum.F001.getValue()) {
            query.put("flag", "NoShowProtal");
        }
        List<FileDO> sysFileList = sysFileService.list(query);
        int total = sysFileService.count(query);
        PageUtils pageUtils = new PageUtils(sysFileList, total);
        return pageUtils;
    }

    @GetMapping("/add")
        // @RequiresPermissions("common:bComments")
    String add() {
        return "common/sysFile/add";
    }

    @GetMapping("/edit")
        // @RequiresPermissions("common:bComments")
    String edit(Long id, Model model) {
        FileDO sysFile = sysFileService.get(id);
        model.addAttribute("sysFile", sysFile);
        return "common/sysFile/edit";
    }

    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
    @RequiresPermissions("common:info")
    public R info(@PathVariable("id") Long id) {
        FileDO sysFile = sysFileService.get(id);
        return R.ok().put("sysFile", sysFile);
    }

    /**
     * 保存
     */
    @ResponseBody
    @PostMapping("/save")
    @RequiresPermissions("common:save")
    public R save(FileDO sysFile) {
        if (sysFileService.save(sysFile) > 0) {
            return R.ok();
        }
        return R.error();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    @RequiresPermissions("common:update")
    public R update(@RequestBody FileDO sysFile) {
        sysFileService.update(sysFile);

        return R.ok();
    }

    /**
     * 删除
     */
    @PostMapping("/remove")
    @ResponseBody
    // @RequiresPermissions("common:remove")
    public R remove(Long id, HttpServletRequest request) {
        if ("test".equals(getUsername())) {
            return R.error(1, "演示系统不允许修改,完整体验请部署程序");
        }

        FileDO fileDO = sysFileService.get(id);

        String url = fileDO.getUrl();
        //删除时url为空直接删除 不删除文件
        if (StringUtils.isBlank(url)) {
            if (sysFileService.remove(id) > 0) {
                return R.ok();
            } else {
                return R.error();
            }
        }
        String fileName = buywangzhanConfig.getUploadPath() + url.replace("/files/", "");

        //删除时 网站正在使用不可删除提示
        Map<String, Object> queryMap = new HashMap();
        queryMap.put("type", FileTypeEnum.F05.getValue());
        queryMap.put("url", url);

        List<FileDO> fileDOs = sysFileService.list(queryMap);
        if (fileDOs != null && fileDOs.size() > 0) {
            return R.error("此图片您的网站仍在使用,故不能删除！");
        }

        if (sysFileService.remove(id) > 0) {
            boolean b = FileUtil.deleteFile(fileName);
            if (!b) {
                return R.error("数据库记录删除成功，文件删除失败");
            }
            return R.ok();
        } else {
            return R.error();
        }
    }

    /**
     * 删除
     */
    @PostMapping("/batchRemove")
    @ResponseBody
    @RequiresPermissions("common:remove")
    public R remove(@RequestParam("ids[]") Long[] ids) {
        if ("test".equals(getUsername())) {
            return R.error(1, "演示系统不允许修改,完整体验请部署程序");
        }
        sysFileService.batchRemove(ids);
        return R.ok();
    }

    @ResponseBody
    @PostMapping("/upload")
    R upload(@RequestParam("file") MultipartFile file, HttpServletRequest request) {
        if ("test".equals(getUsername())) {
            return R.error(1, "演示系统不允许修改,完整体验请部署程序");
        }

        //更新背景图片 参数：方法名，文件完成路径，前端一个上传方法，多次调用
        String method = request.getParameter("method");
        if (StringUtils.isNoneBlank(method) && method.equals("bgpic")) {
            String originalPathName = request.getParameter("originalPathName");
            String filename = buywangzhanConfig.getUploadPath() + originalPathName.replace("/files/", "");
            try {
                synchronized (FileController.class) {
                    boolean b = FileUtil.deleteFile(filename);
                    FileUtil.uploadFile(file.getBytes(), buywangzhanConfig.getUploadPath(), originalPathName.replace("/files/", ""));
                }
            } catch (Exception e) {
                return R.error();
            }
            return R.ok();

        }

        String fileName = file.getOriginalFilename();
        String pathName = FileUtil.RenameToUUID(fileName);
        FileDO sysFile = new FileDO(FileType.fileType(fileName), "/files/" + pathName, new Date(), fileName, null);
        try {
            FileUtil.uploadFile(file.getBytes(), buywangzhanConfig.getUploadPath(), pathName);
        } catch (Exception e) {
            return R.error();
        }

        if (sysFileService.save(sysFile) > 0) {
            return R.ok().put("fileName", sysFile.getUrl()).put("oName", sysFile.getName());
        }
        return R.error();
    }
}
