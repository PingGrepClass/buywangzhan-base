package com.buywangzhan.common.domain;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * 文件上传
 * 
 * @author chglee
 * @email 1992lcg@163.com
 * @date 2017-09-19 16:02:20
 */
public class FileDO implements Serializable {
	private static final long serialVersionUID = 1L;

	//
	private Long id;
	// 文件类型
	private Integer type;
	// URL地址
	private String url;
	// 创建时间
	private Date createDate;
	// 文件原名称
	private String name;
	//文件链接
	private String href;
	private String content;
	private String title;
	private String coordinate;
	private FileDetailDO fileDetailDO;
	private BigDecimal price;
	private BigDecimal discountPrice;
	private String coordinate2;



	public FileDO() {
		super();
	}


	public FileDO(Integer type, String url, Date createDate,String name,String href) {
		super();
		this.type = type;
		this.url = url;
		this.createDate = createDate;
		this.name =name;
		this.href =href;
	}

	/**
	 * 设置：
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * 获取：
	 */
	public Long getId() {
		return id;
	}

	/**
	 * 设置：文件类型
	 */
	public void setType(Integer type) {
		this.type = type;
	}

	/**
	 * 获取：文件类型
	 */
	public Integer getType() {
		return type;
	}

	/**
	 * 设置：URL地址
	 */
	public void setUrl(String url) {
		this.url = url;
	}

	/**
	 * 获取：URL地址
	 */
	public String getUrl() {
		return url;
	}

	/**
	 * 设置：创建时间
	 */
	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	/**
	 * 获取：创建时间
	 */
	public Date getCreateDate() {
		return createDate;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getHref() {
		return href;
	}

	public void setHref(String href) {
		this.href = href;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getCoordinate() {
		return coordinate;
	}

	public void setCoordinate(String coordinate) {
		this.coordinate = coordinate;
	}

	public FileDetailDO getFileDetailDO() {
		return fileDetailDO;
	}

	public void setFileDetailDO(FileDetailDO fileDetailDO) {
		this.fileDetailDO = fileDetailDO;
	}

	public BigDecimal getPrice() {
		return price;
	}

	public void setPrice(BigDecimal price) {
		this.price = price;
	}

	public BigDecimal getDiscountPrice() {
		return discountPrice;
	}

	public void setDiscountPrice(BigDecimal discountPrice) {
		this.discountPrice = discountPrice;
	}

	public String getCoordinate2() {
		return coordinate2;
	}

	public void setCoordinate2(String coordinate2) {
		this.coordinate2 = coordinate2;
	}
}
