package com.buywangzhan.portal.service.impl;

import com.buywangzhan.portal.dao.PortalDao;
import com.buywangzhan.portal.domain.PortalDO;
import com.buywangzhan.portal.service.PContentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;


@Service
public class PContentServiceImpl implements PContentService {
	@Autowired
	private PortalDao bContentMapper;
	
	@Override
	public PortalDO get(Long cid){
		return bContentMapper.get(cid);
	}
	
	@Override
	public List<PortalDO> list(Map<String, Object> map){
		return bContentMapper.list(map);
	}
	
	@Override
	public int count(Map<String, Object> map){
		return bContentMapper.count(map);
	}
	
	@Override
	public int save(PortalDO bContent){
		return bContentMapper.save(bContent);
	}
	
	@Override
	public int update(PortalDO bContent){
		return bContentMapper.update(bContent);
	}
	
	@Override
	public int remove(Long cid){
		return bContentMapper.remove(cid);
	}
	
	@Override
	public int batchRemove(Long[] cids){
		return bContentMapper.batchRemove(cids);
	}
	
}
