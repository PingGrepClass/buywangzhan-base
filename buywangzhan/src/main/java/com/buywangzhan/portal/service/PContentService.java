package com.buywangzhan.portal.service;

import com.buywangzhan.portal.domain.PortalDO;

import java.util.List;
import java.util.Map;

/**
 * 文章内容
 * 
 * @author chglee
 * @email 1992lcg@163.com
 * @date 2017-09-09 10:03:34
 */
public interface PContentService {
	
	PortalDO get(Long cid);
	
	List<PortalDO> list(Map<String, Object> map);
	
	int count(Map<String, Object> map);
	
	int save(PortalDO bContent);
	
	int update(PortalDO bContent);
	
	int remove(Long cid);
	
	int batchRemove(Long[] cids);
}
