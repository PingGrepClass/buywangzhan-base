package com.buywangzhan.portal.dao;

import com.buywangzhan.portal.domain.PortalDO;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

/**
 * 文章内容
 * @author chglee
 * @email 1992lcg@163.com
 * @date 2017-10-03 16:17:48
 */
@Mapper
public interface PortalDao {

	PortalDO get(Long cid);
	
	List<PortalDO> list(Map<String, Object> map);
	
	int count(Map<String, Object> map);
	
	int save(PortalDO content);
	
	int update(PortalDO content);
	
	int remove(Long cid);
	
	int batchRemove(Long[] cids);
}
