package com.buywangzhan.portal.controller;

import com.buywangzhan.blog.domain.ContentDO;
import com.buywangzhan.blog.service.BContentService;
import com.buywangzhan.common.domain.FileDO;
import com.buywangzhan.common.domain.FileDetailDO;
import com.buywangzhan.common.service.FileService;
import com.buywangzhan.common.utils.DateUtils;
import com.buywangzhan.common.utils.PageUtils;
import com.buywangzhan.common.utils.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
public class ProtalViewController {
	@Autowired
	BContentService bContentService;
	@Autowired
	FileService fileService;
	static final String INDEX_DINATE = "a001";//首页页面坐标

    //显示开始

	@GetMapping({"/","index",""})
	String bContent(Model model) {

		Map<String, Object> queryIndex = new HashMap<String, Object>();
		queryIndex.put("coordinate", INDEX_DINATE);
		List<FileDO> indexFiles = fileService.list(queryIndex);

		Map<String, List<FileDO>> preModel = new HashMap<String, List<FileDO>>();
		for (FileDO f : indexFiles) {
			String key = f.getCoordinate2();
			if (preModel.containsKey(key)) {
				List<FileDO> clist = preModel.get(key);
				clist.add(f);
			} else {
				List<FileDO> nlist = new ArrayList<FileDO>();
				nlist.add(f);
				preModel.put(key, nlist);
			}
		}
		String b001Key = "b001";
		List<List<FileDO>> b001List = new ArrayList<List<FileDO>>();
		for (String key : preModel.keySet()) {
			if (key.equals(b001Key)) {
				List<FileDO> preB001List = preModel.get(key);
				int preBoo1Size = preB001List.size();
				if (preBoo1Size > 1 && preBoo1Size % 2 == 0) {
					for (int i = 0; i < preBoo1Size; i++) {
						preB001ToBoo1(b001List, preB001List, i);
					}
				} else if (preBoo1Size > 2 && preBoo1Size % 2 == 1) {
					for (int i = 0; i < preBoo1Size; i++) {
						preB001ToBoo1(b001List, preB001List, i);
					}
				}
				model.addAttribute(b001Key, b001List);
			} else {
				model.addAttribute(key, preModel.get(key));
			}

		}


		return "index";
	}

	private void preB001ToBoo1(List<List<FileDO>> b001List, List<FileDO> preB001List, int i) {

		int bsize = b001List.size();
		if (bsize > i / 2) {
			List<FileDO> b001DoublList = b001List.get(i / 2);
			//doublList 第二个元素
			b001DoublList.add(preB001List.get(i));
		} else {
			List<FileDO> b001DoublList = new ArrayList<FileDO>();
			b001DoublList.add(preB001List.get(i));
			//doublList第一个元素
			b001List.add(b001DoublList);
		}

	}

	@GetMapping("/por/about")
	String about(Model model) {
		String ABOUT_DINATE = "a002";
		setShowMode(ABOUT_DINATE,model);

		return "por/about";
	}
	private void setShowMode(String pageCoordinate ,Model model){

		Map<String, Object> queryIndex = new HashMap<String, Object>();
		queryIndex.put("coordinate", pageCoordinate);
		List<FileDO> Files = fileService.list(queryIndex);

		Map<String, List<FileDO>> preModel = new HashMap<String, List<FileDO>>();
		for (FileDO f : Files) {
			String key = f.getCoordinate2();
			if (preModel.containsKey(key)) {
				List<FileDO> clist = preModel.get(key);
				clist.add(f);
			} else {
				List<FileDO> nlist = new ArrayList<FileDO>();
				nlist.add(f);
				preModel.put(key, nlist);
			}
		}

		for (String key : preModel.keySet()) {
			model.addAttribute(key, preModel.get(key));
		}
	}

	@GetMapping("/por/blog")
	String blog(Model model) {
		String ABOUT_DINATE = "a003";
		setShowMode(ABOUT_DINATE,model);

		return "por/gallery";
	}

	@GetMapping("/por/contact")
	String contact(Model model) {

		String ABOUT_DINATE = "a004";
		setShowMode(ABOUT_DINATE,model);

		return "por/contact";
	}

	@GetMapping("/por/gallery")
	String gallery() {

		return "blog/index/pornews";
	}

	@GetMapping("/por/detail/{detail}/{sysFileId}")
	String post(@PathVariable("detail") String detail,@PathVariable("sysFileId") Long sysFileId, Model model) {
		FileDO fileDO =  fileService.get(sysFileId);
		FileDetailDO fileDetailDO = fileService.getBySysFileId(sysFileId);
		fileDO.setFileDetailDO(fileDetailDO);
		model.addAttribute("fileDO",fileDO);

		return "por/"+detail;
	}

	//显示结束




	@ResponseBody
	@GetMapping("/view/open/list")
	public PageUtils opentList(@RequestParam Map<String, Object> params) {
		// 查询列表数据
		Query query = new Query(params);

		List<ContentDO> bContentList = bContentService.list(query);
		int total = bContentService.count(query);

		PageUtils pageUtils = new PageUtils(bContentList, total);

		return pageUtils;
	}

	@GetMapping("/view/open/post/{cid}")
	String post(@PathVariable("cid") Long cid, Model model) {
		ContentDO bContentDO = bContentService.get(cid);
		model.addAttribute("bContent", bContentDO);
		model.addAttribute("gtmModified", DateUtils.format(bContentDO.getGtmModified()));
		return "blog/index/post";
	}
	@GetMapping("/view/open/page/{categories}")
	String about(@PathVariable("categories") String categories, Model model) {
		Map<String, Object> map = new HashMap<>();
		map.put("categories", categories);
		ContentDO bContentDO = bContentService.list(map).get(0);
		model.addAttribute("bContent", bContentDO);
		return "blog/index/post";
	}
}
