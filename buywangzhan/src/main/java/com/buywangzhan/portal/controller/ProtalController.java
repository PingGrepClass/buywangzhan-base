package com.buywangzhan.portal.controller;

import com.buywangzhan.blog.domain.ContentDO;
import com.buywangzhan.blog.service.BContentService;
import com.buywangzhan.common.utils.DateUtils;
import com.buywangzhan.common.utils.PageUtils;
import com.buywangzhan.common.utils.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RequestMapping("/protal/xxx")
@Controller
public class ProtalController {
	@Autowired
	BContentService bContentService;

	@GetMapping()
	String blog() {
		return "blog/index/main";
	}

	@ResponseBody
	@GetMapping("/open/list")
	public PageUtils opentList(@RequestParam Map<String, Object> params) {
		// 查询列表数据
		Query query = new Query(params);

		List<ContentDO> bContentList = bContentService.list(query);
		int total = bContentService.count(query);

		PageUtils pageUtils = new PageUtils(bContentList, total);

		return pageUtils;
	}

	@GetMapping("/open/post/{cid}")
	String post(@PathVariable("cid") Long cid, Model model) {
		ContentDO bContentDO = bContentService.get(cid);
		model.addAttribute("bContent", bContentDO);
		model.addAttribute("gtmModified", DateUtils.format(bContentDO.getGtmModified()));
		return "blog/index/post";
	}
	@GetMapping("/open/page/{categories}")
	String about(@PathVariable("categories") String categories, Model model) {
		Map<String, Object> map = new HashMap<>();
		map.put("categories", categories);
		ContentDO bContentDO = bContentService.list(map).get(0);
		model.addAttribute("bContent", bContentDO);
		return "blog/index/post";
	}
}
