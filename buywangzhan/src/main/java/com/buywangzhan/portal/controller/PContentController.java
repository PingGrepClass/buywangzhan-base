package com.buywangzhan.portal.controller;

import com.buywangzhan.common.controller.BaseController;
import com.buywangzhan.common.domain.FileDO;
import com.buywangzhan.common.domain.FileDetailDO;
import com.buywangzhan.common.service.FileService;
import com.buywangzhan.common.utils.R;
import com.buywangzhan.system.enums.FileTypeEnum;
import org.apache.commons.lang.StringUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.*;

/**
 * 文章内容
 *
 * @author chglee
 * @email 1992lcg@163.com
 * @date 2017-09-09 10:03:34
 */
@Controller
@RequestMapping("/portal")
public class PContentController extends BaseController {
    @Autowired
    FileService fileService;
    static final String INDEX_DINATE = "a001";//首页页面坐标

    @GetMapping()
    String bContent(Model model) {

        Map<String, Object> queryIndex = new HashMap<String, Object>();
        queryIndex.put("coordinate", INDEX_DINATE);
        List<FileDO> indexFiles = fileService.list(queryIndex);

        Map<String, List<FileDO>> preModel = new HashMap<String, List<FileDO>>();
        for (FileDO f : indexFiles) {
            String key = f.getCoordinate2();
            if (preModel.containsKey(key)) {
                List<FileDO> clist = preModel.get(key);
                clist.add(f);
            } else {
                List<FileDO> nlist = new ArrayList<FileDO>();
                nlist.add(f);
                preModel.put(key, nlist);
            }
        }
        String b001Key = "b001";
        List<List<FileDO>> b001List = new ArrayList<List<FileDO>>();
        for (String key : preModel.keySet()) {
            if (key.equals(b001Key)) {
                List<FileDO> preB001List = preModel.get(key);
                int preBoo1Size = preB001List.size();
                if (preBoo1Size > 1 && preBoo1Size % 2 == 0) {
                    for (int i = 0; i < preBoo1Size; i++) {
                        preB001ToBoo1(b001List, preB001List, i);
                    }
                } else if (preBoo1Size > 2 && preBoo1Size % 2 == 1) {
                    for (int i = 0; i < preBoo1Size; i++) {
                        preB001ToBoo1(b001List, preB001List, i);
                    }
                }
                model.addAttribute(b001Key, b001List);
            } else {
                model.addAttribute(key, preModel.get(key));
            }

        }


        return "portal/index";
    }

    private void preB001ToBoo1(List<List<FileDO>> b001List, List<FileDO> preB001List, int i) {

        int bsize = b001List.size();
        if (bsize > i / 2) {
            List<FileDO> b001DoublList = b001List.get(i / 2);
            //doublList 第二个元素
            b001DoublList.add(preB001List.get(i));
        } else {
            List<FileDO> b001DoublList = new ArrayList<FileDO>();
            b001DoublList.add(preB001List.get(i));
            //doublList第一个元素
            b001List.add(b001DoublList);
        }

    }

    @GetMapping("/about")
    String about(Model model) {
        String ABOUT_DINATE = "a002";
        setShowMode(ABOUT_DINATE,model);

        return "portal/about";
    }
    private void setShowMode(String pageCoordinate ,Model model){

        Map<String, Object> queryIndex = new HashMap<String, Object>();
        queryIndex.put("coordinate", pageCoordinate);
        List<FileDO> Files = fileService.list(queryIndex);

        Map<String, List<FileDO>> preModel = new HashMap<String, List<FileDO>>();
        for (FileDO f : Files) {
            String key = f.getCoordinate2();
            if (preModel.containsKey(key)) {
                List<FileDO> clist = preModel.get(key);
                clist.add(f);
            } else {
                List<FileDO> nlist = new ArrayList<FileDO>();
                nlist.add(f);
                preModel.put(key, nlist);
            }
        }

        for (String key : preModel.keySet()) {
            model.addAttribute(key, preModel.get(key));
        }
    }

    @GetMapping("/blog")
    String blog(Model model) {
        String ABOUT_DINATE = "a003";
        setShowMode(ABOUT_DINATE,model);

        return "portal/gallery";
    }

    @GetMapping("/contact")
    String contact(Model model) {

        String ABOUT_DINATE = "a004";
        setShowMode(ABOUT_DINATE,model);

        return "portal/contact";
    }

    @GetMapping("/gallery")
    String gallery() {

        return "blog/index/news";
    }

//	@ResponseBody
//	@GetMapping("/list")
//	@RequiresPermissions("blog:bContent:bContent")
//	public PageUtils list(@RequestParam Map<String, Object> params) {
//		Query query = new Query(params);
//		List<ContentDO> bContentList = bContentService.list(query);
//		int total = bContentService.count(query);
//		PageUtils pageUtils = new PageUtils(bContentList, total);
//		return pageUtils;
//	}
//
//	@GetMapping("/add")
//	@RequiresPermissions("blog:bContent:add")
//	String add() {
//		return "blog/bContent/add";
//	}
//
//	@GetMapping("/edit/{cid}")
//	@RequiresPermissions("blog:bContent:edit")
//	String edit(@PathVariable("cid") Long cid, Model model) {
//		ContentDO bContentDO = bContentService.get(cid);
//		model.addAttribute("bContent", bContentDO);
//		return "blog/bContent/edit";
//	}
//

    /**
     * 保存
     */
    @ResponseBody
    @RequiresPermissions("blog:bContent:add")
    @PostMapping("/save")
    public R save(FileDO fileDO) {
        try {
            if ("test".equals(getUsername())) {
                return R.error(1, "演示系统不允许修改,完整体验请部署程序");
            }
            if(null!=checkFileDo(fileDO)){
                return checkFileDo(fileDO);
            }

            if (null == fileDO.getType()) {
                fileDO.setType(FileTypeEnum.F05.getValue());
            }




            int count;
            if (fileDO.getId() == null || fileDO.getId() == 0) {
                fileDO.setCreateDate(new Date());
                FileDetailDO fileDetailDO =fileDO.getFileDetailDO();
                if (fileDetailDO!= null) {
                    if(StringUtils.isBlank(fileDO.getHref())){
                        if(StringUtils.isNotBlank(fileDetailDO.getUrl())&&StringUtils.isNotBlank(fileDetailDO.getUrl2())&&StringUtils.isNotBlank(fileDetailDO.getUrl3())&&StringUtils.isNotBlank(fileDetailDO.getUrl4())&&StringUtils.isNotBlank(fileDetailDO.getUrl5())){
                            fileDO.setHref("/portal/detail/detail5/"+fileDO.getId());
                        }else if(StringUtils.isNotBlank(fileDetailDO.getUrl())&&StringUtils.isNotBlank(fileDetailDO.getUrl2())&&StringUtils.isNotBlank(fileDetailDO.getUrl3())){
                            fileDO.setHref("/portal/detail/detail3/"+fileDO.getId());
                        }
                    }
                    fileDetailDO.setCreateDate(new Date());
                }

                count = fileService.save(fileDO);
            } else {
                count = fileService.update(fileDO);
            }
            if (count > 0) {
                return R.ok();
            }
            return R.error();
        } catch (Exception e) {
            return R.error();
        }
    }

    private R checkFileDo(FileDO fileDo){
        if(fileDo==null|| (StringUtils.isBlank(fileDo.getUrl()) &&StringUtils.isBlank(fileDo.getTitle()))){
            return R.error(1,"图片和标题不能同时为空！");
        }
        return  null;
    }

    @GetMapping("/detail/{detail}/{sysFileId}")
    String post(@PathVariable("detail") String detail,@PathVariable("sysFileId") Long sysFileId, Model model) {
          FileDO fileDO =  fileService.get(sysFileId);
          FileDetailDO fileDetailDO = fileService.getBySysFileId(sysFileId);
        fileDO.setFileDetailDO(fileDetailDO);
          model.addAttribute("fileDO",fileDO);

        return "portal/"+detail;
    }


    @ResponseBody
    @RequiresPermissions("blog:bContent:add")
    @PostMapping("/query")
    public FileDO query(FileDO fileDO) {
        try {
            if ("test".equals(getUsername())) {
                return null;
            }
            FileDO file = null;
            if (fileDO != null && fileDO.getId() != null) {
                file = fileService.get(fileDO.getId());
                if (file != null) {
                    FileDetailDO fileDetail = fileService.getBySysFileId(fileDO.getId());
                    if (fileDetail != null) {
                        file.setFileDetailDO(fileDetail);
                    }
                }

            }
            return file;
        } catch (Exception e) {
            return null;
        }
    }

    /**
     * 修改
     */
    @ResponseBody
    @RequiresPermissions("blog:bContent:edit")
    @PostMapping("/update")
    public R update(FileDO fileDO) {

        if ("test".equals(getUsername())) {
            return R.error(1, "演示系统不允许修改,完整体验请部署程序");
        }

        if(null!=checkFileDo(fileDO)){
            return checkFileDo(fileDO);
        }

        int count = fileService.update(fileDO);
        if (count > 0) {
            return R.ok();
        }
        return R.error();
    }

    /**
     * 删除
     */
    @RequiresPermissions("blog:bContent:remove")
    @PostMapping("/remove")
    @ResponseBody
    public R remove(FileDO fileDO) {
        if ("test".equals(getUsername())) {
            return R.error(1, "演示系统不允许修改,完整体验请部署程序");
        }
        try {


            Map<String, Object> queryCount = new HashMap<>();
            queryCount.put("coordinate", fileDO.getCoordinate());
            queryCount.put("coordinate2", fileDO.getCoordinate2());
            int count = fileService.count(queryCount);

            if(fileDO.getCoordinate2().equals("b001")){
                if (count < 3) {
                    return R.error(1, "主图或内容不能低于模板数量！");
                }
            }else{
                if (count < 2) {
                    return R.error(1, "主图或内容不能低于模板数量！");
                }

            }


            if (fileService.remove(fileDO.getId()) > 0) {
                return R.ok();
            }
            return R.error();
        } catch (Exception e) {
            return R.error();
        }

    }

//	/**
//	 * 删除
//	 */
//	@RequiresPermissions("blog:bContent:batchRemove")
//	@PostMapping("/batchRemove")
//	@ResponseBody
//	public R remove(@RequestParam("ids[]") Long[] cids) {
//		if ("test".equals(getUsername())) {
//			return R.error(1, "演示系统不允许修改,完整体验请部署程序");
//		}
//		bContentService.batchRemove(cids);
//		return R.ok();
//	}
}
