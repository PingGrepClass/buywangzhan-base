package com.buywangzhan.system.enums;

/**
 * Created by Administrator on 2017/10/30.
 */
public enum FileTypeEnum {

    F001("全部文件",-1),F00("图片",0),F01("文档",1),F02("视频",2),F03("音乐",3),F05("Portal文件",5),F99("其他",99);

    FileTypeEnum(String lable,Integer value){
        this.lable =lable;
        this.value =value;
    };
    private String lable;
    private Integer value;
    public  Integer getValue(){
      return this.value;
    }

    public  String getLable(){
        return this.lable;
    }

}
