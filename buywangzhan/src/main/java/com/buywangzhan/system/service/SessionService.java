package com.buywangzhan.system.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.buywangzhan.system.domain.UserOnline;

@Service
public interface SessionService {
	List<UserOnline> list();
}
